import UIKit
import DropDown

class TableViewCell: UITableViewCell {

    //Create constant drop down to display in "More" button
    let dropDown = DropDown()
    
    @IBOutlet weak var imagePost: UIImageView!
    @IBOutlet weak var durationPost: UILabel!
    @IBOutlet weak var usernamePost: UILabel!
    @IBOutlet weak var profilePost: UIImageView!
    @IBOutlet weak var captionPost: UILabel!
    @IBOutlet weak var share: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var like: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //Set profile picture to circle
        profilePost.layer.cornerRadius = profilePost.frame.height/2
        
        //Set border color of profile picture to green
        profilePost.layer.borderColor = UIColor.systemGreen.cgColor
        
        //Set width of profile picture border
        profilePost.layer.borderWidth = 2
        
        //Set username to bold
        usernamePost.font = UIFont.boldSystemFont(ofSize: usernamePost.font.pointSize)
        
    }
    
    @IBAction func btnMore(_ sender: UIButton) {
        
        //Set data display in drop down menu
        dropDown.dataSource = ["Save", "Hide", "Unfollow", "Block", "Report"]
            dropDown.anchorView = sender
            dropDown.bottomOffset = CGPoint(x: 0, y: sender.frame.size.height)
            dropDown.show()
}
    //Create function to pass data from UINib to View Controller
    func configure(imagePost: UIImage, durationPost: String, usernamePost: String, profilePost: UIImage, captionPost: String, share: String, comment: String, like: String){
        
        self.captionPost?.text = captionPost
        self.comment?.text = comment
        self.durationPost?.text = durationPost
        self.imagePost?.image = imagePost
        self.share?.text = share
        self.comment?.text = comment
        self.usernamePost?.text = usernamePost
        self.profilePost?.image = profilePost
        self.like?.text = like
        
    }

}
