//
//  StatusTableViewCell.swift
//  iOS-NGETSONYTA-HW-003
//
//  Created by Nyta on 12/1/20.
//

import UIKit

class StatusTableViewCell: UITableViewCell {
    
    //Create static identifier
    static var identifier = "statusCell"
    
    @IBOutlet weak var durationPost: UILabel!
    @IBOutlet weak var usernamePost: UILabel!
    @IBOutlet weak var profilePost: UIImageView!
    @IBOutlet weak var captionPost: UILabel!
    @IBOutlet weak var share: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var like: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //Set profile picture to circle
        profilePost.layer.cornerRadius = profilePost.frame.height/2
        
        //Set border color of profile picture to green
        profilePost.layer.borderColor = UIColor.systemGreen.cgColor
        
        //Set width of profile picture border
        profilePost.layer.borderWidth = 2
        
        //Set username to bold
        usernamePost.font = UIFont.boldSystemFont(ofSize: usernamePost.font.pointSize)
        
    }
    
    //Create static function of UINib
    static func nib() -> UINib {
        return UINib(nibName: "StatusTableViewCell", bundle: nil)
    }
    
    //Create func to pass data from UINib to View Controller
    func configureStatus(durationPost: String, usernamePost: String, profilePost: UIImage, captionPost: String, share: String, comment: String, like: String){
        
        self.captionPost?.text = captionPost
        self.comment?.text = comment
        self.durationPost?.text = durationPost
        self.share?.text = share
        self.comment?.text = comment
        self.usernamePost?.text = usernamePost
        self.profilePost?.image = profilePost
        
    }
}
