//
//  HomeViewController.swift
//  iOS-NGETSONYTA-HW-003
//
//  Created by Nyta on 11/27/20.
//

import UIKit

class HomeViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    @IBOutlet weak var status: UITextField!
    @IBOutlet weak var myProfile: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    var imagePicker = UIImagePickerController()
    
    //Empty Post
    var objPost = [Post]()
    
    //Empty Story
    var objStor = [Story]()
    
    //Create single post
    let post1 = Post(user: User(name: "nichaphatc", profile: UIImage(named: "lyka")!), duration: "Just Now", caption: "ขอ❤️หน่อยคร้าบบบบ คริคริ", image: UIImage(named: "salyna"), amountLike: "22 likes", amountComment: "10 comments", amountShare: "2K shares")
    let post2 = Post(user: User(name: "kenzie", profile: UIImage(named: "davika")!), duration: "Just Now", caption: "I’m numb pls take care of me 😭😳", image: UIImage(named: "miss"), amountLike: "22 likes", amountComment: "10 comments", amountShare: "2K shares")
    
    let post3 = Post(user: User(name: "katylove_s", profile: UIImage(named: "na")!), duration: "1 min", caption: "Comfortable in this set🏃‍♀️", image: UIImage(named: "nana"), amountLike: "22 likes", amountComment: "10 comments", amountShare: "2K shares")
    let post4 = Post(user: User(name: "davikah", profile: UIImage(named: "yaya")!), duration: "5 mins", caption: "កូនខ្ញុំអេីយ​ កំពូលស្រលាញ់សត្វ​ តែកុំមេីលទឹកមុខអូន😆🐶🐒 Kem Seimorokot_MOMO #momo2yearsold", image: UIImage(named: "sreymom"), amountLike: "22 likes", amountComment: "10 comments", amountShare: "2K shares")
    let post5 = Post(user: User(name: "urassayas", profile: UIImage(named: "la")!), duration: "40 mins", caption: "ថ្ងៃនេះឆ្លងខែជាមួយគ្នា ចុះខែក្រោយឆ្លងឆ្នាំជាមួយគ្នាបានអត់✨✨✨... ⁉️", image: UIImage(named: "sakura"), amountLike: "22 likes", amountComment: "10 comments", amountShare: "2K shares")
    let post6 = Post(user: User(name:"ខ្យង ស្រែ", profile: UIImage(named: "lyka")!), duration: "1 hr", caption: "Designed for anyone interested in web development, Here are some more details of what you'll learn😮❤️💯", image: UIImage(named: "universe"), amountLike: "22 likes", amountComment: "10 comments", amountShare: "2K shares")
    
    let post7 = Post(user: User(name: "sakurashidaa", profile: UIImage(named: "yaya")!), duration: "2 hr", caption: "Designed for anyone interested in web development, Here are some more details of what you'll learn😮❤️💯", image: nil, amountLike: "22 likes", amountComment: "10 comments", amountShare: "2K shares")
    
    
    
    //Create single story
    let story1 = Story(user: User(name: "Sethneary", profile: UIImage(named: "na")!), image: UIImage(named: "sreymom")!)
    
    let story2 = Story(user: User(name: "Somanith", profile: UIImage(named: "vivi")!), image: UIImage(named: "lyka")!)
    
    let story3 = Story(user: User(name: "Sreynin", profile: UIImage(named: "na")!), image: UIImage(named: "tiger")!)
    
    let story4 = Story(user: User(name: "Rosaliroath", profile: UIImage(named: "stella")!), image: UIImage(named: "miss")!)
    let story5 = Story(user: User(name: "Anna", profile: UIImage(named: "vivi")!), image: UIImage(named: "kaka")!)
    
    let story6 = Story(user: User(name: "Bunleang", profile: UIImage(named: "la")!), image: UIImage(named: "nana")!)
    
    let story7 = Story(user: User(name: "Dalin", profile: UIImage(named: "yaya")!), image: UIImage(named: "universe")!)
    
    let story8 = Story(user: User(name: "Leaphy", profile: UIImage(named: "davika")!), image: UIImage(named: "sakura")!)

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Append multiple object's stories into Story
        objStor.append(contentsOf: [story1, story2, story3, story4, story5, story6, story7, story8])
        
        //Append multiple object's posts into Post
        objPost.append(contentsOf: [post1, post2, post3, post4, post5, post6, post7])
   
        //Set UI Bar Item into black color
        UITabBar.appearance().tintColor = UIColor.black
        
        //Disable selection in tableViewCell(post)
        tableView.allowsSelection = false
        
        //Disable verticle scroll bar in tabeleView(post)
        tableView.showsVerticalScrollIndicator = false
        
        //Disable horizontal scroll ber in collectionView(story)
        collectionView.showsHorizontalScrollIndicator = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        //Set profile picture to circle
        myProfile.layer.cornerRadius = myProfile.frame.height/2
        
        //Set layer of status("What's on your mind?")
        status.layer.cornerRadius = status.frame.height/2
        
        //Register Nib of TableViewCell(post)
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        //Register Nib of TableViewCell(status)
        tableView.register(StatusTableViewCell.nib(), forCellReuseIdentifier: StatusTableViewCell.identifier)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //Register Nib of CollectionView(story)
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")

        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    //Button Live
    @IBAction func btnLive(_ sender: UIButton) {
        let alert = UIAlertController(title: "Live", message: "You have clicked Live button.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in
        })) //Cancel Action
        
        
        alert.addAction(UIAlertAction(title: "Live", style: .default,
        handler: {(_: UIAlertAction!) in
        })) //Live action
        self.present(alert, animated: true, completion: nil)

    }
    //Button Photo
    @IBAction func btnPhoto(_ sender: Any) {
        
        //Display photo to choose from gallary
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")

            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //Function choosing photo in gallary
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
            self.dismiss(animated: true, completion: { () -> Void in
            })
        }
    
    //Button CheckIn
    @IBAction func btnCheckIn(_ sender: UIButton) {
        let alert = UIAlertController(title: "Check In", message: "You have clicked CheckIn button.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in
        })) //Cancel Action
        
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default,
        handler: {(_: UIAlertAction!) in
        })) //Okay action
        self.present(alert, animated: true, completion: nil)

    }
}
extension HomeViewController : UITableViewDelegate, UITableViewDataSource{
    
    //Number of cell(row) to display in TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objPost.count
    }
    
    //Display data in each cell(row)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        let statusCell = tableView.dequeueReusableCell(withIdentifier: StatusTableViewCell.identifier, for: indexPath) as! StatusTableViewCell
        
        //Create constant of data in each Post
        let namee = objPost[indexPath.row].user.name
        let captionn = objPost[indexPath.row].caption
        let durationn = objPost[indexPath.row].duration
        let profilee = objPost[indexPath.row].user.profile
        let likee = objPost[indexPath.row].amountLike
        let commentt = objPost[indexPath.row].amountComment
        let sharee = objPost[indexPath.row].amountShare
        
        //Check condition of image whether or not it's nil
        if let img = objPost[indexPath.row].image{
            cell.imagePost.image = img
            cell.configure(imagePost: img, durationPost: durationn, usernamePost: namee, profilePost: profilee, captionPost: captionn, share: sharee, comment: commentt, like: likee)
            return cell
        }else {
            statusCell.configureStatus(durationPost: durationn, usernamePost: namee, profilePost: profilee, captionPost: captionn, share: sharee, comment: commentt, like: likee)
            return statusCell
        }

    }
    
}

extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    
    //Number of cell(item) to display
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objStor.count
    }
    
    //Display data in each cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        cell.nameStory.text = objStor[indexPath.row].user.name
        cell.imageStory.image = objStor[indexPath.row].image
        cell.profileStory.image = objStor[indexPath.row].user.profile
        
        return cell
    }
}
extension HomeViewController : UICollectionViewDelegateFlowLayout{
    
    //Set Size of Collection View Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 500)
    }
    
}
