//
//  CollectionViewCell.swift
//  iOS-NGETSONYTA-HW-003
//
//  Created by Nyta on 11/30/20.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameStory: UILabel!
    @IBOutlet weak var profileStory: UIImageView!
    @IBOutlet weak var imageStory: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
        profileStory.layer.cornerRadius = profileStory.frame.height/2
        
        profileStory.layer.borderWidth = 2
        profileStory.layer.borderColor = UIColor.systemBlue.cgColor
        
        imageStory.layer.cornerRadius = 20
        
    }

}
