
import Foundation
import UIKit

struct Post{
    var user: User
    var duration: String
    var caption: String
    var image: UIImage?
    var amountLike: String
    var amountComment: String
    var amountShare: String
    
}

