
import Foundation
import UIKit

struct User {
    var name: String
    var profile: UIImage
}
